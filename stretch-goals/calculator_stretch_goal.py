def calculator(a, b, operator):
    # ==============
    # Your code here
    total = 0
    if operator == "+":
        total = int(a + b)
        return bin(total).replace("0b", "")
    if operator == "-":
        total = int(a-b)
        return bin(total).replace("0b", "")
    if operator == "*":
        total = a * b
    else:
        total = int(a / b)

    return bin(total).replace("0b", "")

    # ==============


print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
