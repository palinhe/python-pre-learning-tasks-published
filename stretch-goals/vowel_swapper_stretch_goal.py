def vowel_swapper(string):
    # ==============
    # Your code here
    myList = list(string)
    counterA = 0
    counterE = 0
    counterI = 0
    counterO = 0
    counterU = 0

    for i in range(len(myList)):
        #A
        if myList[i] == "a" or myList[i] == "A":
            if counterA == 1:
                myList[i] = "4"
                counterA = 0
            elif counterA == 0:
                counterA = 1
        #E
        if myList[i] == "e" or myList[i] == "E":
            if counterE == 1:
                myList[i] = "3"
                counterE = 0
            elif counterE == 0:
                counterE = 1
        #I
        if myList[i] == "i" or myList[i] == "I":
            if counterI == 1:
                myList[i] = "!"
                counterI = 0
            elif counterI == 0:
                counterI = 1
        #o
        if myList[i] == "o":
            if counterO == 1:
                myList[i] = "ooo"
                counterO = 0
            elif counterO == 0:
                counterO = 1
        #O
        if myList[i] == "O":
            if counterO == 1:
                myList[i] = "000"
                counterO = 0
            elif counterO == 0:
                counterO = 1
        #U
        if myList[i] == "u" or myList[i] == "U":
            if counterU == 1:
                myList[i] = "|_|"
                counterU = 0
            elif counterU == 0:
                counterU = 1
    string = "".join(myList)
    return string
    # ==============

#Followed Git instrucstions on changing characters since in the py file for stretch goals it changed
print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a/\a e3e i!i o000o u\/u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av/\!lable" to the console
