def factors(number):
    # ==============
    # Your code here
    factorList = []
    for i in range(1, number + 1):
        if number % i == 0 and number != i and i != 1:
            factorList.append(i)

    if len(factorList) == 0:
        return print(number, "is a prime number")
    else:
        return factorList


    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
